<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Card;
use App\Repository\CardRepository;
use Symfony\Component\HttpFoundation\Request;

class MainController extends AbstractController
{
    /**
     * @Route("/play", name="play")
     */
    public function play(CardRepository $oCardRep, Request $oReq): Response
    {
        $iCards = $oReq->get('iCards');
        $aRandomCards = [];
        $aOrdredCards = [];
        // retourner un tableau d'objets cards pas trié
        if(!null == $iCards){
          $aRandoms = array_rand($oCardRep->findAll(), $iCards);
          foreach ($aRandoms as $iRandom) {
            $aRandomCards[] = $oCardRep->find($iRandom);
          }
          // trier le tableau de cartes
          foreach ($aRandomCards as $oCard) {
            // TODO : refactor
            $prioColor = $oCard->getColor()->getPrio();
            $prioOrdre = $oCard->getOrdre()->getPrio();
            $iPriorities = $prioColor + $prioOrdre;

            $aOrdredCards[$iPriorities] = $oCard;
          }
        }
        // dd($aOrdredCards);
        return $this->render('main/play.html.twig', [
            'unorderCards' => $aRandomCards,
            'orderCards' => $aOrdredCards,
        ]);
    }
}
