<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Card;
use App\Entity\Ordre;
use App\Entity\Color;

class AddCardsCommand extends Command
{
    protected static $defaultName = 'addCards';
    // protected static $defaultDescription = 'Add a short description for your command';
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }
    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $em = $this->entityManager;

        $aColors = $em->getRepository("App:Color")->findAll();
        $aOrdre = $em->getRepository("App:Ordre")->findAll();

        foreach($aColors as $oColor){
          foreach($aOrdre as $oOrdre) {
          $oCard = new Card();
          $oCard->setColor($oColor);
          $oCard->setOrdre($oOrdre);
          $em->persist($oCard);
          $em->flush();
          }
        }
        $io->success('Vos carte ont été bien crées ! ');

        return Command::SUCCESS;
    }
}
