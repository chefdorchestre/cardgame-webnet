<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Color;

class AddColorsCommand extends Command
{
    protected static $defaultName = 'add colors';
    protected static $defaultDescription = 'Add a short description for your command';
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }
    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $em = $this->entityManager;
        $aColors = [
          ['name' => 'carreau','prio' => 1],
          ['name' => 'coeur','prio' => 2],
          ['name' => 'pique','prio' => 3],
          ['name' => 'trefle','prio' => 4],
        ];

        foreach ($aColors as $aColor) {
          $oColor = new Color();
          $oColor->setName($aColor['name']);
          $oColor->setPrio($aColor['prio']);
          $em->persist($oColor);
          $em->flush();
        }
        $io->success('Vos couleurs ont été bien crées ! ');

        return Command::SUCCESS;
    }
}
