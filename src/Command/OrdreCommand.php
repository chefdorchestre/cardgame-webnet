<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Ordre;

class OrdreCommand extends Command
{
    protected static $defaultName = 'ordre';
    protected static $defaultDescription = 'Add a short description for your command';
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }
    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $em = $this->entityManager;
//  AS, 10, 8, 6, 5, 7, 4, 2, 3, 9, Dame, Roi, Valet
        $aOrdres = [
          ['name' => 'as','prio' => 1],
          ['name' => '10','prio' => 2],
          ['name' => '8','prio' => 3],
          ['name' => '6','prio' => 4],
          ['name' => '5','prio' => 5],
          ['name' => '7','prio' => 6],
          ['name' => '4','prio' => 7],
          ['name' => '2','prio' => 8],
          ['name' => '3','prio' => 9],
          ['name' => '9','prio' => 10],
          ['name' => 'dame','prio' => 11],
          ['name' => 'roi','prio' => 12],
          ['name' => 'valet','prio' => 13],
        ];

        foreach ($aOrdres as $aOrdre) {
          $oOrder = new Ordre();
          $oOrder->setName($aOrdre['name']);
          $oOrder->setPrio($aOrdre['prio']);
          $em->persist($oOrder);
          $em->flush();
        }

        $io->success('Les ordres ont été bien crées');

        return Command::SUCCESS;
    }
}
