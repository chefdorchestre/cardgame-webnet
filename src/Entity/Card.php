<?php

namespace App\Entity;
use App\Entity\Ordre;
use App\Entity\Color;

use App\Repository\CardRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CardRepository::class)
 */
class Card
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=ordre::class, inversedBy="cards")
     */
    private $ordre;

    /**
     * @ORM\ManyToOne(targetEntity=color::class, inversedBy="cards")
     */
    private $color;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrdre(): ?ordre
    {
        return $this->ordre;
    }

    public function setOrdre(?ordre $ordre): self
    {
        $this->ordre = $ordre;

        return $this;
    }

    public function getColor(): ?color
    {
        return $this->color;
    }

    public function setColor(?color $color): self
    {
        $this->color = $color;

        return $this;
    }
}
